import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('loading', () => {
  const is1Loading = ref(false)
  const doLoad = () => {
    is1Loading.value = true
  }
  const finish = () => {
    is1Loading.value = false
  }

  return { is1Loading,doLoad,finish }
})
